package com.adam.pupper.data.apimodels

import com.adam.pupper.data.ResponseStatus
import com.adam.pupper.data.exceptions.GenericPupException
import com.google.gson.annotations.SerializedName
import com.squareup.picasso.Picasso

data class BreedImageResponseDto(
    @SerializedName(value = "status")
    val status: String,
    @SerializedName(value = "message")
    val message: String,
    @SerializedName(value = "code")
    val code: Int?,
) {
    private fun getResponseType() = ResponseStatus.toResponseStatus(status)

    fun getImageUrl(): String {
        when (getResponseType()) {
            ResponseStatus.SUCCESS -> {
                return message
            }
            ResponseStatus.ERROR -> {
                throw GenericPupException(message = message)
            }
        }
    }
}