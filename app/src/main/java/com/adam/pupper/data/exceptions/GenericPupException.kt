package com.adam.pupper.data.exceptions

class GenericPupException(
    message: String
) : Throwable(message)