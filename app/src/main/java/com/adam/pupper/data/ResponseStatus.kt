package com.adam.pupper.data

enum class ResponseStatus {
    SUCCESS,
    ERROR;

    companion object {
        fun toResponseStatus(code: String) = when (code) {
            "success" -> SUCCESS
            "error" -> ERROR
            else -> throw IllegalArgumentException("unknown code:$code")
        }
    }
}