package com.adam.pupper.data

import com.adam.pupper.data.apimodels.BreedImageResponseDto
import com.adam.pupper.data.apimodels.HomeResponseWrapperDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface WebService {

    @GET(value = "breeds/list/all")
    fun getBreedList(): Single<HomeResponseWrapperDto>

    @GET(value = "breed/{breed_name}/images/random")
    fun getBreedImageUrl(
        @Path("breed_name") breedName: String
    ): Single<BreedImageResponseDto>

}