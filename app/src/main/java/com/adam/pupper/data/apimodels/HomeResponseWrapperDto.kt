package com.adam.pupper.data.apimodels

import com.adam.pupper.data.ResponseStatus
import com.adam.pupper.data.exceptions.GenericPupException
import com.google.gson.annotations.SerializedName

data class HomeResponseWrapperDto(
    @SerializedName(value = "status")
    val status: String,
    @SerializedName(value = "message")
    val message: Map<String, Any>,
    @SerializedName(value = "code")
    val code: Int?,
) {
    private fun getResponseType() = ResponseStatus.toResponseStatus(status)

    fun getBreedList(): List<String> {
        when (getResponseType()) {
            ResponseStatus.SUCCESS -> {
                return message.keys.toList()
            }
            ResponseStatus.ERROR -> {
                throw GenericPupException(message = "error occurred")
            }
        }
    }
}