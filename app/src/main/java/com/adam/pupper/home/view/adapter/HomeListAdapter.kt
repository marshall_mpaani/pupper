package com.adam.pupper.home.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.adam.pupper.databinding.ItemBreedHeaderViewBinding
import com.adam.pupper.databinding.ItemBreedViewBinding
import com.adam.pupper.home.uimodel.HomeBreedHeaderElement
import com.adam.pupper.home.uimodel.HomeBreedNameElement
import com.adam.pupper.home.uimodel.HomeUiElementMarker
import io.reactivex.subjects.PublishSubject

class HomeListAdapter :
    ListAdapter<
            HomeUiElementMarker,
            RecyclerView.ViewHolder>
        (
        HomeUiElementMarker.DIFF_UTIL_IMPL
    ) {

    companion object {
        private const val VIEW_TYPE_HEADER = 12
        private const val VIEW_TYPE_VALUE = 13
    }

    val clickInteraction: PublishSubject<HomeUiElementMarker> = PublishSubject.create()

    override
    fun getItemViewType(position: Int) = when (
        val model = getItem(position)
    ) {
        is HomeBreedHeaderElement -> VIEW_TYPE_HEADER
        is HomeBreedNameElement -> VIEW_TYPE_VALUE
        else -> throw IllegalArgumentException(
            "unKnown ViewType${model.javaClass.simpleName}"
        )
    }

    override
    fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_HEADER -> ItemBreedHeaderViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).run(::HomeListHeaderViewHolder)
            VIEW_TYPE_VALUE -> ItemBreedViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).run(::HomeListViewHolder)
            else -> throw IllegalArgumentException(
                "unKnown ViewType${viewType}"
            )
        }
    }

    override
    fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        when (holder) {
            is HomeListHeaderViewHolder -> {
                holder.bind(model = getItem(position) as HomeBreedHeaderElement)
            }
            is HomeListViewHolder -> {
                holder.bind(model = getItem(position) as HomeBreedNameElement)
            }
        }
    }

    inner
    class HomeListHeaderViewHolder(
        private val binding: ItemBreedHeaderViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: HomeBreedHeaderElement) {
            binding.tvBreedHeader.text = model.breedElementHeader.toString()
        }
    }

    inner
    class HomeListViewHolder(
        private val binding: ItemBreedViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: HomeBreedNameElement) {
            binding.tvBreedName.text = model.breedName
            binding.root.setOnClickListener { clickInteraction.onNext(model) }
        }
    }
}
