package com.adam.pupper.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import com.adam.pupper.home.di.HomeListDependencyHolder
import com.adam.pupper.home.model.HomeListRepoC
import com.adam.pupper.home.uimodel.HomeUiModel
import com.adam.pupper.imagedialog.uimodel.BreedImageWrapperHomeUiModel
import io.reactivex.BackpressureStrategy
import io.reactivex.disposables.CompositeDisposable

class HomeListViewModel(
    private val repo: HomeListRepoC.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    val breedListLiveData: LiveData<HomeUiModel> by lazy {
        LiveDataReactiveStreams.fromPublisher(
            repo.breedList.toFlowable(BackpressureStrategy.LATEST)
        )
    }

    val breedImageLiveData: LiveData<BreedImageWrapperHomeUiModel> by lazy {
        LiveDataReactiveStreams.fromPublisher(
            repo.breedImageUrl.toFlowable(BackpressureStrategy.LATEST)
        )
    }

    fun getBreedList(
        force: Boolean
    ) {
        if (force) {
            repo.getBreedList()
        }
    }

    fun getBreedImageUrl(
        force: Boolean,
        breedName: String
    ) {
        if (force) {
            repo.getBreedImageUrl(
                breedName = breedName
            )
        }
    }

    override
    fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        HomeListDependencyHolder.destroyComponent()
    }
}