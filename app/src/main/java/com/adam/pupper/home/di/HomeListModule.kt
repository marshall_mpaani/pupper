package com.adam.pupper.home.di

import android.content.Context
import com.adam.pupper.data.WebService
import com.adam.pupper.home.model.HomeListRemote
import com.adam.pupper.home.model.HomeListRepo
import com.adam.pupper.home.model.HomeListRepoC
import com.adam.pupper.home.viewmodel.HomeListVMF
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

@Module
class HomeListModule {

    @Provides
    fun providesVMF(
        repo: HomeListRepoC.Repository,
        compositeDisposable: CompositeDisposable,
    ): HomeListVMF {
        return HomeListVMF(
            repo = repo,
            compositeDisposable = compositeDisposable,
        )
    }

    @Provides
    fun providesRepo(
        remote: HomeListRepoC.Remote,
        compositeDisposable: CompositeDisposable,
    ): HomeListRepoC.Repository = HomeListRepo(
        remote = remote,
        compositeDisposable = compositeDisposable,
    )

    @Provides
    fun provideRemote(
        webService: WebService
    ): HomeListRepoC.Remote = HomeListRemote(webService = webService)

    @Provides
    fun providesWebService(retrofit: Retrofit): WebService = retrofit.create(WebService::class.java)

    @Provides
    fun providesCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}