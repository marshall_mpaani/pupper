package com.adam.pupper.home.uimodel

data class HomeBreedHeaderElement(
    override val recordId: Int,
    val breedElementHeader: Char,
) : HomeUiElementMarker {
    constructor(
        breedElementHeader: Char
    ) : this(
        recordId = breedElementHeader.hashCode(),
        breedElementHeader = breedElementHeader,
    )
}