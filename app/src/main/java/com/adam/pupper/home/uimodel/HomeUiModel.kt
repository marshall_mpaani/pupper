package com.adam.pupper.home.uimodel

import com.adam.pupper.util.processingstates.State

data class HomeUiModel(
    val state: State<List<HomeUiElementMarker>>
)