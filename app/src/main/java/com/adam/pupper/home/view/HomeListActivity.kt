package com.adam.pupper.home.view

import android.content.Context
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import com.adam.pupper.databinding.ActivityHomeListBinding
import com.adam.pupper.home.di.HomeListDependencyHolder
import com.adam.pupper.home.uimodel.HomeBreedNameElement
import com.adam.pupper.home.view.adapter.HomeListAdapter
import com.adam.pupper.home.viewmodel.HomeListVMF
import com.adam.pupper.home.viewmodel.HomeListViewModel
import com.adam.pupper.imagedialog.view.BreedImageDialog
import com.adam.pupper.util.processingstates.State
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class HomeListActivity : AppCompatActivity() {

    private
    val binding: ActivityHomeListBinding by lazy {
        ActivityHomeListBinding.inflate(layoutInflater)
    }

    private
    val context: Context by lazy { this }

    private
    val mRvAdapter by lazy { HomeListAdapter() }

    private
    lateinit var uiSubscription: Disposable

    @Inject
    lateinit var vmf: HomeListVMF

    private
    val viewModel by viewModels<HomeListViewModel> { vmf }

    override
    fun onCreate(savedInstanceState: Bundle?) {
        HomeListDependencyHolder
            .initListComponent()
            .inject(activity = this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setUpUi()
        setLiveDataListeners()
        fetchData(force = savedInstanceState == null)
    }

    private
    fun setUpUi() {
        binding.rvHomeBreedList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        binding.rvHomeBreedList.adapter = mRvAdapter
        binding.incError.btnRetry.setOnClickListener {
            fetchData(force = true)
        }
        uiSubscription = mRvAdapter.clickInteraction
            .subscribe {
                if (it is HomeBreedNameElement) {
                    BreedImageDialog.newInstance(uiModel = it)
                        .show(
                            supportFragmentManager,
                            BreedImageDialog.TAG
                        )
                }
            }
    }

    private
    fun fetchData(
        force: Boolean = false
    ) = viewModel.getBreedList(
        force = force
    )

    private
    fun setLiveDataListeners() {
        viewModel.breedListLiveData.observeForever {
            when (it.state) {
                is State.Loading -> {
                    binding.sfProgress.isVisible = true
                    binding.incError.root.isGone = true
                    binding.rvHomeBreedList.isGone = true
                }
                is State.Data -> {
                    binding.sfProgress.isGone = true
                    binding.rvHomeBreedList.isVisible = true
                    binding.incError.root.isGone = true
                    mRvAdapter.submitList(it.state.data)
                }
                is State.Error -> {
                    binding.rvHomeBreedList.isGone = true
                    binding.sfProgress.isGone = true
                    binding.incError.root.isVisible = true
                    binding.incError.errorGroup.isVisible = true
                }
            }
        }
    }

    override
    fun onDestroy() {
        super.onDestroy()
        uiSubscription.dispose()
    }

    companion object {
        @Suppress("unused")
        private const val TAG = "MainActivity"
    }

}