package com.adam.pupper.home.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.adam.pupper.home.model.HomeListRepoC
import io.reactivex.disposables.CompositeDisposable

class HomeListVMF(
    private val repo: HomeListRepoC.Repository,
    private val compositeDisposable: CompositeDisposable,
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override
    fun <T : ViewModel?> create(modelClass: Class<T>): T =
        HomeListViewModel(repo = repo, compositeDisposable = compositeDisposable) as T
}