package com.adam.pupper.home.model

import com.adam.pupper.home.uimodel.HomeBreedHeaderElement
import com.adam.pupper.home.uimodel.HomeBreedNameElement
import com.adam.pupper.home.uimodel.HomeUiElementMarker
import com.adam.pupper.home.uimodel.HomeUiModel
import com.adam.pupper.imagedialog.uimodel.BreedImageHomeUiModel
import com.adam.pupper.imagedialog.uimodel.BreedImageWrapperHomeUiModel
import com.adam.pupper.util.processingstates.State.Companion.publishData
import com.adam.pupper.util.processingstates.State.Companion.publishError
import com.adam.pupper.util.processingstates.State.Companion.publishLoading
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class HomeListRepo(
    private val remote: HomeListRepoC.Remote,
    private val compositeDisposable: CompositeDisposable,
) : HomeListRepoC.Repository {

    override
    val breedList: PublishSubject<HomeUiModel> = PublishSubject.create()

    override
    val breedImageUrl: PublishSubject<BreedImageWrapperHomeUiModel> = PublishSubject.create()

    override
    fun getBreedList() {
        breedList.onNext(HomeUiModel(state = publishLoading()))
        remote.getBreedList()
            .map { remoteModel ->
                remoteModel.getBreedList()
                    .map(::HomeBreedNameElement)
                    .groupBy { it.breedName.first() }
                    .map {
                        mutableListOf<HomeUiElementMarker>().apply {
                            add(HomeBreedHeaderElement(it.key.toUpperCase()))
                            addAll(it.value)
                        }
                    }.flatten()
            }
            .map { HomeUiModel(state = publishData(data = it)) }
            .onErrorReturn { HomeUiModel(state = publishError(e = it)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(breedList::onNext)
            .run(compositeDisposable::add)
    }

    override
    fun getBreedImageUrl(breedName: String) {
        breedImageUrl.onNext(BreedImageWrapperHomeUiModel(state = publishLoading()))
        remote.getBreedImageUrl(
            breedName = breedName
        ).map { remoteModel ->
            BreedImageWrapperHomeUiModel(
                state = publishData(
                    data = BreedImageHomeUiModel(
                        breedName = breedName,
                        imageUrl = remoteModel.getImageUrl()
                    )
                )
            )
        }
            .onErrorReturn { BreedImageWrapperHomeUiModel(state = publishError(it)) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(breedImageUrl::onNext)
            .run(compositeDisposable::add)
    }
}