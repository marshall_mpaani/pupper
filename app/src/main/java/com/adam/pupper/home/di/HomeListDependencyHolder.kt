package com.adam.pupper.home.di

import com.adam.pupper.application.App

object HomeListDependencyHolder {

    private var listComponent: HomeListComponent? = null

    fun initListComponent(): HomeListComponent {
        if (listComponent == null) {
            listComponent = DaggerHomeListComponent.builder()
                .coreComponent(App.coreComponent)
                .build()
        }
        return listComponent as HomeListComponent
    }

    fun destroyComponent() {
        listComponent = null
    }

}