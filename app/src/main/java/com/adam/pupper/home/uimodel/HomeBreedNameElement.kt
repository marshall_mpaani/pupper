package com.adam.pupper.home.uimodel

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HomeBreedNameElement(
    override val recordId: Int,
    val breedName: String,
) : HomeUiElementMarker, Parcelable {
    constructor(
        breedName: String
    ) : this(
        recordId = breedName.hashCode(),
        breedName = breedName,
    )
}