package com.adam.pupper.home.uimodel

import androidx.recyclerview.widget.DiffUtil

interface HomeUiElementMarker {
    val recordId: Int

    companion object {
        val DIFF_UTIL_IMPL = object : DiffUtil.ItemCallback<HomeUiElementMarker>() {
            override fun areItemsTheSame(
                oldItem: HomeUiElementMarker,
                newItem: HomeUiElementMarker
            ): Boolean = oldItem == newItem

            override fun areContentsTheSame(
                oldItem: HomeUiElementMarker,
                newItem: HomeUiElementMarker
            ): Boolean = oldItem.recordId == newItem.recordId
        }
    }
}