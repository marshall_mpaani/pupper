package com.adam.pupper.home.di

import com.adam.pupper.application.di.CoreComponent
import com.adam.pupper.home.view.HomeListActivity
import dagger.Component

@HomeListScope
@Component(
    dependencies = [CoreComponent::class],
    modules = [HomeListModule::class]
)
interface HomeListComponent {
    fun inject(activity: HomeListActivity)
}