package com.adam.pupper.home.model

import com.adam.pupper.data.WebService
import io.reactivex.Single

class HomeListRemote(
    private val webService: WebService
) : HomeListRepoC.Remote {

    override
    fun getBreedList() = webService.getBreedList()

    override
    fun getBreedImageUrl(breedName: String) =
        webService.getBreedImageUrl(breedName = breedName)
}