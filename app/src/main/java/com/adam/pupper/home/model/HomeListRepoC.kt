package com.adam.pupper.home.model

import com.adam.pupper.data.apimodels.BreedImageResponseDto
import com.adam.pupper.data.apimodels.HomeResponseWrapperDto
import com.adam.pupper.home.uimodel.HomeUiModel
import com.adam.pupper.imagedialog.uimodel.BreedImageWrapperHomeUiModel
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface HomeListRepoC {

    interface Repository {
        val breedList: PublishSubject<HomeUiModel>
        val breedImageUrl: PublishSubject<BreedImageWrapperHomeUiModel>
        fun getBreedList()
        fun getBreedImageUrl(breedName: String)
    }

    interface Local

    interface Remote {
        fun getBreedList(): Single<HomeResponseWrapperDto>
        fun getBreedImageUrl(breedName: String): Single<BreedImageResponseDto>
    }
}