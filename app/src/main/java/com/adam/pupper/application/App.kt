package com.adam.pupper.application

import androidx.multidex.MultiDexApplication
import com.adam.pupper.BuildConfig
import com.adam.pupper.application.di.CoreComponent
import com.adam.pupper.application.di.CoreModule
import com.adam.pupper.application.di.DaggerCoreComponent
import com.facebook.stetho.Stetho
import com.squareup.picasso.Picasso
import javax.inject.Inject

class App : MultiDexApplication() {

    companion object {
        lateinit var coreComponent: CoreComponent
    }

    @Inject
    lateinit var picasso: Picasso

    override fun onCreate() {
        super.onCreate()
        coreComponent = DaggerCoreComponent.builder()
            .coreModule(CoreModule(context = this))
            .build()
        coreComponent.inject(this)

        initialisePicasso()
        initialiseStetho()
    }

    private fun initialiseStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(applicationContext)
        }
    }

    private fun initialisePicasso() {
        // To make sure Picasso (with Okhttp3Downloader) is initialized only once
        Picasso.setSingletonInstance(picasso)
    }
}