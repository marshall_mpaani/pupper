package com.adam.pupper.application.di

import android.content.Context
import com.adam.pupper.application.App
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        CoreModule::class,
        StorageModule::class,
        NetworkModule::class
    ]
)
interface CoreComponent {

    fun context(): Context
    fun retrofit(): Retrofit
    fun inject(app: App)
}