package com.adam.pupper.imagedialog.uimodel

data class BreedImageHomeUiModel(
    val breedName: String,
    val imageUrl: String,
)