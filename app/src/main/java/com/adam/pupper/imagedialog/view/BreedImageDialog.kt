package com.adam.pupper.imagedialog.view

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.adam.pupper.databinding.DialogImageViewBinding
import com.adam.pupper.home.uimodel.HomeBreedNameElement
import com.adam.pupper.home.viewmodel.HomeListViewModel
import com.adam.pupper.util.processingstates.State
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class BreedImageDialog : DialogFragment() {

    private
    lateinit var uiModel: HomeBreedNameElement

    private
    val viewModel: HomeListViewModel by activityViewModels()

    private
    lateinit var binding: DialogImageViewBinding

    override
    fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        setUpUi()
        setUpLiveDataListener()
        fetchData(force = savedInstanceState == null)
    }

    private
    fun setUpLiveDataListener() {
        viewModel.breedImageLiveData.observe(this) {
            when (it.state) {
                is State.Loading -> {
                    binding.ivImage.setImageResource(android.R.color.transparent)
                }
                is State.Data -> {
                    if (it.state.data.breedName != uiModel.breedName) {
                        return@observe
                    }
                    Picasso.with(context)
                        .load(it.state.data.imageUrl)
                        .into(binding.ivImage, object : Callback {
                            override fun onSuccess() = Unit
                            override fun onError() = showErrorMessage()
                        })
                }
                is State.Error -> {
                    showErrorMessage()
                }
            }
        }
    }

    private
    fun showErrorMessage() {
        binding.ivImage.setImageResource(android.R.drawable.ic_dialog_alert)
        Toast.makeText(context, "error occurred", Toast.LENGTH_SHORT).apply {
            setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        }.show()
    }

    private
    fun fetchData(
        force: Boolean
    ) {
        viewModel.getBreedImageUrl(
            force = force,
            breedName = uiModel.breedName
        )
    }

    private
    fun setUpUi() {
        binding.btnDismiss.setOnClickListener { dismiss() }
    }

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        uiModel = arguments?.getParcelable(ARGS_BREED_DATA_SET)
            ?: throw RuntimeException("dataSet not found")
    }

    override
    fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogImageViewBinding.inflate(
            layoutInflater,
            container,
            false
        )
        return binding.root
    }

    override
    fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    companion object {

        const val TAG = "BigImageDialog"
        private const val ARGS_BREED_DATA_SET = "breed-data-set"

        fun newInstance(
            uiModel: HomeBreedNameElement
        ) =
            BreedImageDialog().apply {
                arguments = bundleOf(
                    ARGS_BREED_DATA_SET to uiModel
                )
            }
    }
}