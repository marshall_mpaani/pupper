object Config {
    const val appId = "com.adam.pupper"

    const val debug_minifyEnabled = false

    const val release_minifyEnabled = true

    const val kotlinVersion = "1.4.21"
}

object AndroidSdk {
    const val min = 19
    const val compile = 30
    const val target = compile
}

object Urls {
    const val baseUrl = "https://dog.ceo/api/"
}

object Libraries {
    private object Versions {
        const val jetpack = "1.2.0"
        const val constraintLayout = "2.0.4"
        const val swiperefreshlayout = "1.1.0"
        const val recyclerview = "1.1.0"
        const val materialComponent = "1.2.1"
        const val preference = "1.1.1"
        const val ktx = "1.3.2"
        const val fragmentKtx = "1.2.5"
        const val okhttp = "4.9.0"
        const val retrofit = "2.9.0"
        const val picasso = "2.5.2"
        const val rxjava = "2.2.21"
        const val rxAndroid = "2.1.1"
        const val gson = "2.8.6"
        const val gsonConverter = "2.9.0"
        const val retrofitRxAdapter = "2.9.0"
        const val picassoDownloader = "1.1.0"
        const val ktxLifecycleViewmodel = "2.2.0"
        const val reactivestreamsViewModel = "2.2.0"
        const val ktxViewModelRuntime = "2.2.0"
        const val ktxViewModelLifeCycleCompiler = "2.2.0"
        const val stetho = "1.5.1"
        const val dagger = "2.33"
        const val multidex = "2.0.1"

        const val facebookShimmerSdk = "0.5.0"


    }

    val stetho = "com.facebook.stetho:stetho:${Versions.stetho}"
    val stethoNetworkInterceptor = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho}"
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Config.kotlinVersion}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.jetpack}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val swiperefreshlayout =
        "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swiperefreshlayout}"
    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    const val materialComponent =
        "com.google.android.material:material:${Versions.materialComponent}"
    const val preference = "androidx.preference:preference-ktx:${Versions.preference}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktx}"
    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerAndroid = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAndroidCompiler =
        "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"
    const val picassoDownloader =
        "com.jakewharton.picasso:picasso2-okhttp3-downloader:${Versions.picassoDownloader}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    const val rxjava = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
    const val retrofitRxAdapter =
        "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitRxAdapter}"
    const val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentKtx}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.gsonConverter}"

    const val ktxViewModelLifeCycleCompiler =
        "androidx.lifecycle:lifecycle-compiler:${Versions.ktxViewModelLifeCycleCompiler}"
    const val reactivestreamsViewModel =
        "androidx.lifecycle:lifecycle-reactivestreams:${Versions.reactivestreamsViewModel}"
    const val ktxLifecycleViewmodel =
        "androidx.lifecycle:lifecycle-extensions:${Versions.ktxLifecycleViewmodel}"
    const val ktxViewModelRuntime =
        "androidx.lifecycle:lifecycle-runtime:${Versions.ktxViewModelRuntime}"
    const val facebookShimmer = "com.facebook.shimmer:shimmer:${Versions.facebookShimmerSdk}"
    const val multidex = "androidx.multidex:multidex:${Versions.multidex}"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val extJunit = "1.1.1"
        const val test_okhttp = "4.2.2"
        const val test_liveData = "2.1.0"
        const val espresso = "3.1.0"
        const val mockito = "1.+"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val extJunit = "androidx.test.ext:junit:${Versions.extJunit}"
    const val test_okhttp = "com.squareup.okhttp3:mockwebserver:${Versions.test_okhttp}"
    const val test_liveData = "androidx.arch.core:core-testing:${Versions.test_liveData}"
    const val espresso =
        "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val mockito = "org.mockito:mockito-core:${Versions.mockito}"

}